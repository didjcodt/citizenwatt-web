#! /bin/bash

# First, start the InfluxDB time series database
docker stop influxdb

# Finally, start the grafana frontend
docker stop grafana

# And stop the ECO2Mix grabber
docker stop citizenwatt/eco2mix:0.1

# Stop the Open Weather Map grabber
docker stop citizenwatt/owmap:0.1

docker rm influxdb
docker rm grafana
docker rm eco2mix
docker rm owmap
