/// <reference path="../../../../public/app/headers/common.d.ts" />
export declare function tablePanelEditor(): {
    restrict: string;
    scope: boolean;
    templateUrl: string;
    link: (scope: any, elem: any) => void;
};
