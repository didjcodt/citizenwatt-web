#!/usr/bin/python
from influxdb import InfluxDBClient
import requests
import io
from io import BytesIO, StringIO
import zipfile
import math
import datetime
import pytz
import time
import csv
import urllib
import threading

USER = 'root'
PASSWORD = 'root'
DBNAME = 'eCO2mix'
client = InfluxDBClient('influxdb', 8086, USER, PASSWORD, DBNAME)

def check_connectivity(reference):
	request = requests.get(reference)
	print("Status code : " + str(request.status_code))
#	print(request.text)

def checkIf(a): 
	if(a is not None and len(a)>0):
		return a

def importHistoricalData(fromDate, toDate):
	'''
	Import all the eCO2mix data from fromDate to toDate in the InfluxDB
	date format : string "DD/MM/YYYY"
	'''
	day1 = fromDate.split("/")
	d1 = datetime.date(int(day1[2]), int(day1[1]), int(day1[0]))
	day2 = toDate.split("/")
	d2 = datetime.date(int(day2[2]), int(day2[1]), int(day2[0]))
	
	delta = d2-d1
	for i in range(delta.days + 1):
		rangday = d1 + datetime.timedelta(days = i)
		url = "http://eco2mix.rte-france.com/curves/eco2mixDl?date=" + u'%02d' % rangday.day +"/" + u'%02d' % rangday.month + "/" + str(rangday.year)
		print(url)
		importECO2MixData(url)

def importECO2MixData(url="http://eco2mix.rte-france.com/download/eco2mix/eCO2mix_RTE_En-cours-TR.zip"): 
	#Download the file
	request = requests.get(url)
	file = zipfile.ZipFile(BytesIO(request.content))
	data = io.TextIOWrapper(file.open(file.filelist[0].filename), encoding='ISO8859')
	json = []
	reader = csv.DictReader(data, delimiter='\t')
	for raw in reader: 
		r1 = raw['Date']
		r2 = raw['Heures']
		if(r1 is None or r2 is None or len(r1)==0 or len(r2)==0):
			break
		regtime = r1 + 'T' + r2
		local = pytz.timezone("Europe/Paris")
		try: 
			naive = datetime.datetime.strptime(regtime, "%Y-%m-%dT%H:%M")
			local_dt = local.localize(naive, is_dst=None)
		except pytz.exceptions.NonExistentTimeError: 
			print("[!] Oops, RTE is making sh*t")
		except pytz.exceptions.AmbiguousTimeError: 
			print("[!] Stop making sh*t please !!!!")

		utc_dt = local_dt.astimezone(pytz.utc)
		regtime = utc_dt.strftime("%Y-%m-%dT%H:%M")
		# This is for the timestamp str(time.mktime(datetime.datetime.strptime(raw[2]+' '+raw[3], "%Y-%m-%d %H:%M").timetuple()))[:-2]
		consumption, oil, coal, gaz, nuclear, wind, solar, hydraulic, pumping, bioenergies, co2 = [], [], [], [], [], [], [], [], [], [], [] 
		if(checkIf(raw['Consommation'])): 
			json.append({
				"measurement": 'consumption', 
				"time": regtime, 
				"fields": {
					"value": int(raw['Consommation'])
				}})
		if(checkIf(raw['Fioul'])): 
			json.append({
				"measurement": 'oil',
				"time": regtime,
				"fields": {
					"value": int(raw['Fioul'])
				}})
		if(checkIf(raw['Charbon'])): 
			json.append({
				"measurement": 'coal',
				"time": regtime,
				"fields": {
					"value": int(raw['Charbon'])
				}})
		if(checkIf(raw['Gaz'])): 
			json.append({
				"measurement": 'gaz',
				"time": regtime,
				"fields": {
					"value": int(raw['Gaz'])
				}})
		if(checkIf(raw['Nucl\xe9aire'])): 
			json.append({
				"measurement": 'nuclear',
				"time": regtime,
				"fields": {
					"value": int(raw['Nucl\xe9aire'])
				}})
		if(checkIf(raw['Eolien'])):
			json.append({
				"measurement": 'wind',
				"time": regtime,
				"fields": {
					"value": int(raw['Eolien'])
				}})
		if(checkIf(raw['Solaire'])):
			json.append({
				"measurement": 'solar',
				"time": regtime,
				"fields": {
					"value": int(raw['Solaire'])
				}})
		if(checkIf(raw['Hydraulique'])):
			json.append({
				"measurement": 'hydraulic',
				"time": regtime,
				"fields": {
					"value": int(raw['Hydraulique'])
				}})
		if(checkIf(raw['Pompage'])):
			json.append({
				"measurement": 'pumping',
				"time": regtime,
				"fields": {
					"value": int(raw['Pompage'])
				}})
		if(checkIf(raw['Bio\xe9nergies'])): 
			json.append({
				"measurement": 'bioenergies',
				"time": regtime,
				"fields": {
					"value": int(raw['Bio\xe9nergies'])
				}})
		if(checkIf(raw['Taux de Co2'])): 
			json.append({
				"measurement": 'co2',
				"time": regtime,
				"fields": {
					"value": int(raw['Taux de Co2'])
				}})

		#json.append([consumption, oil, coal, gaz, nuclear, wind, solar, hydraulic, pumping, bioenergies, co2])
	
	#Flatten the json
	#json = [item for sublist in json for item in sublist]
	client.write_points(json)

def main(host='5.196.74.55', port=8086):
	"""
	main function
	"""
	check_connectivity("http://perdu.com")
	importECO2MixData()
	#importHistoricalData("20/10/2015", "15/12/2015")
	print(time.ctime())
	threading.Timer(900, main).start()

if __name__ == '__main__':
	main()
