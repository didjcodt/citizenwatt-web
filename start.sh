#! /bin/bash
 
# Start the InfluxDB time series database
docker run -d --restart always -p 8083:8083  -p 8086:8086 -v /opt/citizenwatt/influxdb/config:/config -v /opt/citizenwatt/influxdb/data:/data --name influxdb   tutum/influxdb

# Start the grafana frontend
docker run -d --restart always -p 3000:3000  -v /opt/citizenwatt/grafana/varLibGrafana/grafana:/var/lib/grafana -v /opt/citizenwatt/grafana/usrShareGrafana:/usr/share/grafana -v /opt/citizenwatt/grafana/etcGrafana:/etc/grafana -v /opt/citizenwatt/grafana/logs:/var/logs/grafana --link influxdb:influxdb   --name grafana   grafana/grafana

# TODO: when a grabber crashes, it is needed to relaunch the docker

# Start the ECO2Mix grabber
docker build -t citizenwatt/eco2mix:0.1 eCO2MixDocker
docker run -d --restart always --link influxdb:influxdb citizenwatt/eco2mix:0.1

# Start the Open Weather Map grabber
docker build -t citizenwatt/owmap:0.1 OWMDocker
docker run -d --restart always --link influxdb:influxdb citizenwatt/owmap:0.1
