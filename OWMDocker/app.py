#!/usr/bin/python
from influxdb import InfluxDBClient
import requests
from io import BytesIO
import zipfile
import math
import datetime
import time
import csv
import urllib
import threading

# {"_id":6455259,"name":"Paris","country":"FR","coord":{"lon":2.35236,"lat":48.856461}}

USER = 'root'
PASSWORD = 'root'
DBNAME = 'OpenWeatherMap'
client = InfluxDBClient('influxdb', 8086, USER, PASSWORD, DBNAME)

def check_connectivity(reference):
	request = requests.get(reference)
	print("Status code : " + str(request.status_code))
#	print(request.text)

def importHistoricalData(fromDate, toDate):
	'''
	Import all the OWMap station number 137218 data from fromDate to toDate in the InfluxDB
	date format : string "DD/MM/YYYY"
	'''
	day1 = fromDate.split("/")
	d1 = datetime.date(int(day1[2]), int(day1[1]), int(day1[0]))
	day2 = toDate.split("/")
	d2 = datetime.date(int(day2[2]), int(day2[1]), int(day2[0]))
	
	delta = d2-d1
	for i in range(delta.days + 1):
		rangday = d1 + datetime.timedelta(days = i)
		url = "http://eco2mix.rte-france.com/curves/eco2mixDl?date=" + u'%02d' % rangday.day +"/" + u'%02d' % rangday.month + "/" + str(rangday.year)
		print(url)
		importOWMData(url)

def importOWMData(url="http://api.openweathermap.org/data/2.5/weather?id=6455259&appid=33b8ad35e4a59f172586c543c313566a"): 
	#Download the file
	request = requests.get(url)
	#Get the json string
	jsonString = request.json()

	j = []

	#Parse the calculation time
	timestamp = jsonString['dt']
	regtime = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%dT%H:%M:%S')
	#Parse the weather id
	j.append({
		"measurement": 'weather', 
		"time": regtime, 
		"fields": {
			"value": int(jsonString['weather'][0]['id'])
		}})
	mainJson = jsonString['main']
	#Parse Humidity
	j.append({
		"measurement": 'humidity', 
		'time': regtime,
		"fields": {
			"value": int(mainJson['humidity'])
		}})
	#Parse Pressure
	j.append({
                "measurement": 'pressure',
                'time': regtime,
                "fields": {
                        "value": int(mainJson['pressure'])
                }})
	#Parse Temperature
	j.append({
                "measurement": 'temperature',
                'time': regtime,
                "fields": {
                        "value": float(mainJson['temp'] - 273.15)
                }})
	#Parse Wind speed
	j.append({
                "measurement": 'windspeed',
                'time': regtime,
                "fields": {
                        "value": float(jsonString['wind']['speed'])
                }})
	#Parse Clouds
	j.append({
                "measurement": 'clouds',
                'time': regtime,
                "fields": {
                        "value": int(jsonString['clouds']['all'])
                }})
	
	client.write_points(j)

def main(host='5.196.74.55', port=8086):
	"""
	main function
	"""
	check_connectivity("http://perdu.com")
	importOWMData()
	#importHistoricalData("01/01/2015", "12/12/2015")
	print(time.ctime())
	threading.Timer(300, main).start()

if __name__ == '__main__':
	main()
